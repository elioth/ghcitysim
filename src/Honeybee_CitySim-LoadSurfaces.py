﻿# GH-CitySim: an interface to CitySim started by Giuseppe Peronato
#
# © All rights reserved. Ecole polytechnique fédérale de Lausanne (EPFL), Switzerland,
# Laboratory of Integrated Performance in Design (LIPID), 2016-2018
# Developer: Giuseppe Peronato <giuseppe.peronato@alumni.epfl.ch>

# Further development conducted at Uppsala University, Sweden.
# Division of Construction Engineering, 2019
# Developer: Giuseppe Peronato <giuseppe.peronato@angstrom.uu.se>

# Elioth (Egis Concept), France
# 2019-2020
# Developer: Giuseppe Peronato <g.peronato@elioth.fr>

# CitySim is a software developed and distributed by the
# Laboratory of Solar Energy and Building Physics (LESO-PB)
# http://citysim.epfl.ch/
#
# This component relies on Honeybee: A Plugin for Environmental Analysis (GPL) started by Mostapha Sadeghipour Roudsari

"""
This component loads the Citysim ouput into Grasshopper.

-
This component will hopefully be part of
Ladybug: A Plugin for Environmental Analysis (GPL) started by Mostapha Sadeghipour Roudsari

@license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>

-

    
    Args:
        directory: Directory
        name: name of the project
        resType: Choose between SW (Shortwave Irradiation), TS (surface temperature)
        cumulative: calculate cumulative values
        _HBZones: Import _HBZones
        Run: set Boolean to True to load the results
    Returns:
        values: Tree of output values {Building;Surface}. The last two branches contain Shading and Terrain surfaces
        geometry: Tree of input geometry {Building;Surface} without ground floors, adiabatic surfaces and reversed shading surfaces
        objtypes: Object type (Surface, Shading or Terrain) with the same tree structure of the other outputs
        
"""

ghenv.Component.Name = "Honeybee_CitySim-LoadSurfaces"
ghenv.Component.NickName = 'CitySim-LoadSurfaces'
ghenv.Component.Message = 'VER 0.0.1\nAVR_13_2020'
ghenv.Component.Category = "Honeybee"
ghenv.Component.SubCategory = "14 | CitySim"
#compatibleHBVersion = VER 0.0.67\nNOV_20_2018
#compatibleLBVersion = VER 0.0.64\nDEC_04_2018
try: ghenv.Component.AdditionalHelpFromDocStrings = "2"
except: pass

import rhinoscriptsyntax as rs
import ghpythonlib as gh
import scriptcontext as sc
import uuid
import array as arr
hb_hive = sc.sticky["honeybee_Hive"]()

geometry = []
if dir != None:
    dir += "\\" #Add \ in case is missing

HBO = hb_hive.callFromHoneybeeHive(_HBZones)



def list_to_tree(input, none_and_holes=True, source=[0]):
    """Transforms nestings of lists or tuples to a Grasshopper DataTree"""
    # written by Giulio Piacentino, giulio@mcneel.com
    from Grasshopper import DataTree as Tree
    from Grasshopper.Kernel.Data import GH_Path as Path
    from System import Array
    def proc(input,tree,track):
        path = Path(Array[int](track))
        if len(input) == 0 and none_and_holes: tree.EnsurePath(path); return
        for i,item in enumerate(input):
            if hasattr(item, '__iter__'): #if list or tuple
                track.append(i); proc(item,tree,track); track.pop()
            else:
                if none_and_holes: tree.Insert(item,path,i)
                elif item is not None: tree.Add(item,path)
    if input is not None: t=Tree[object]();proc(input,t,source[:]);return t
    
    
def loadOut(path,name,type="SW"):
    #Load SW output file
    in_file = open(path+name+"_"+type+".out","r")
    txt = in_file.read()
    in_file.close()
    results = txt.splitlines()
    header = results[0]
    results.pop(0) #remove header
    return header, results

def parseHead(head):        
    #Parse header
    import re
    header = head.split()
    #header.pop(0)
    bIDs = []
    sIDs = []
    for i in range(len(header)):
        if i == 0:
            bIDs.append('')
            sIDs.append('')
        else:
            b = header[i].split('(')
            s = re.search('(?<=:)\w+', header[i])
            b = b[0]
            if s != None:
                s = s.group(0)
                #print 'building', b, 'surface', s
                bIDs.append(b)
                sIDs.append(int(s))
    return bIDs, sIDs
    
def parseRes(results,sIDs,IDs):
    #Parse results
    irrH = []
    for l in results: #for each line corresponding to a hour
        bldg = []
        isrf = []
        for i in l.split(): #split the surfaces
            isrf.append(float(i))
        irrH.append(isrf)
    irr = []
    irry = []
    types = []
    dictIDs = {}
    for s in xrange(len(IDs)): #for each surface
            srf = []
            for h in range(len(irrH)): #for each hourf
                srf.append(irrH[h][s])
            irr.append(srf)
            irry.append(sum(srf))
            dictIDs[str(IDs[s])+"-"+str(sIDs[s])] = s
            if IDs[s] != "NA" and IDs[s] != "" and ")" not in IDs[s]: #Buildings:
                types.append("Building")
            elif IDs[s] == "NA": #Terrain
                types.append("Surface")
            elif ")" in IDs[s]: #Tree
                types.append("Tree")
            else:
                types.append("Unknown")
    return irr, irry, types, dictIDs


if Run:
    header, results = loadOut(dir,name,resType.upper())
    IDs, sIDs = parseHead(header)
    irr, irry, types, dictIDs = parseRes(results,sIDs,IDs)
    values = []
    geometry = []
    objtypes = []
    i = 1
    for b,building in enumerate(HBO):
#        if building.bldgProgram != "person":
            bvalues = []
            bgeom = []
            objtype = []
            for s,surface in enumerate(building.surfaces):
                if ((surface.type < 2.0) or (surface.type > 3.0)) and surface.BC == "Outdoors":
                    if types[i] == "Building":
                        key = str(b)+"-"+str(s)
                        if cumulative:
                                bvalues.append([irry[dictIDs.get(key,-9999)]])
                        else:
                                bvalues.append(irr[dictIDs.get(key,-9999)])
                        bgeom.append([surface.geometry])
                        if building.bldgProgram == "person":
                            objtype.append(["person"])
                        else:
                            objtype.append(["building"])
                        i += 1
            values.append(bvalues)
            geometry.append(bgeom)
            objtypes.append(objtype)


    terrain = []
    shading = []
    nshading = 0
    for mesh in Shading:
        faces = gh.components.FaceBoundaries(mesh)
        if not isinstance(faces, list):
            faces = [faces]
        for face in faces:
            shading.append([face])
            nshading += 1
    for mesh in Terrain:
        faces = gh.components.FaceBoundaries(mesh)
        if not isinstance(faces, list):
            faces = [faces]
        for face in faces:
            terrain.append([face])
    geometry.append(shading)
    geometry.append(terrain)
    
    count = 0
    terrain = []
    shading = []
    for s,surface in enumerate(sIDs):
         if types[s] == "Surface":
             if int(sIDs[s]) < 99999: #exclude reversed faces
                 if count < nshading:
                    if cumulative:
                        shading.append([irry[s]])
                    else:
                        shading.append(irr[s])
                 else:
                    if cumulative:
                        terrain.append([irry[s]])
                    else:
                        terrain.append(irr[s])
                 count += 1
    values.append(shading)
    values.append(terrain[::-1])
    objtypes.append([["Shading"] for x in shading])
    objtypes.append([["Terrain"] for x in terrain])

    values = list_to_tree(values,none_and_holes=True, source=[])
    geometry = list_to_tree(geometry,none_and_holes=True, source=[])
    objtypes = list_to_tree(objtypes,none_and_holes=True, source=[])