﻿# GH-CitySim: an interface to CitySim started by Giuseppe Peronato
#
# © All rights reserved. Ecole polytechnique fédérale de Lausanne (EPFL), Switzerland,
# Laboratory of Integrated Performance in Design (LIPID), 2016-2018
# Developer: Giuseppe Peronato <giuseppe.peronato@alumni.epfl.ch>

# This component is provided by Elioth (Egis Concept)
# Developer: Giuseppe Peronato <g.peronato@elioth.fr>
# November 2019-June 2020

# CitySim is a software developed and distributed by the
# Laboratory of Solar Energy and Building Physics (LESO-PB)
# http://citysim.epfl.ch/

"""
This component creates a human thermal zone for outdoor confort modeling
based on the method developed by Dr. Silvia Coccolo 
http://dx.doi.org/10.5075/epfl-thesis-7756
-
This component will hopefully be part of
Ladybug: A Plugin for Environmental Analysis (GPL) started by Mostapha Sadeghipour Roudsari

@license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
"""


import rhinoscriptsyntax as rs
import Rhino as rc
import ghpythonlib.components as gc
import scriptcontext as sc
import os
import sys
import System
import Grasshopper.Kernel as gh
import uuid
tolerance = sc.doc.ModelAbsoluteTolerance
import math

ghenv.Component.Name = 'Honeybee_CitySim-Human'
ghenv.Component.NickName = 'CitySim-Human'
ghenv.Component.Message = 'VER 0.0.03\nJUN_16_2020'
ghenv.Component.IconDisplayMode = ghenv.Component.IconDisplayMode.application
ghenv.Component.Category = "Honeybee"
ghenv.Component.SubCategory = "14 | CitySim"
#compatibleHBVersion = VER 0.0.57\nNOV_04_2016
#compatibleLBVersion = VER 0.0.59\nFEB_01_2015
try: ghenv.Component.AdditionalHelpFromDocStrings = "3"
except: pass

# Geometry creation
if type == "MRT":
    z = 1
else:
    z = 0

location = rc.Geometry.Point3d(locations[0],locations[1],locations[2]+z)
circle = rc.Geometry.Circle(location,0.085)

octagone = rc.Geometry.Polyline.ToPolylineCurve(rc.Geometry.Polyline.CreateInscribedPolygon(circle,8))
octagone = gc.Rotate(octagone,math.radians(22.5),gc.XYPlane(location)).geometry

vector = gc.UnitZ(1.5 - z)
base = rc.Geometry.Brep.CreatePlanarBreps(octagone)[0]
top = gc.Move(base,vector).geometry

faces = [base,top]
for segment in gc.Explode(octagone,recursive=True).segments:
    faces.append(rc.Geometry.PlaneSurface.CreateExtrusion(segment,vector).ToBrep())

person = gc.BrepJoin(faces).breps


# The following coded is adapted from Honeybee_createHBSurfaces and Honeybee_createHBZones
def main(geometry, EPConstruction):
    # import the classes
    if sc.sticky.has_key('honeybee_release'):

        try:
            if not sc.sticky['honeybee_release'].isCompatible(ghenv.Component): return -1
            if sc.sticky['honeybee_release'].isInputMissing(ghenv.Component): return -1
        except:
            warning = "You need a newer version of Honeybee to use this compoent." + \
            "Use updateHoneybee component to update userObjects.\n" + \
            "If you have already updated userObjects drag Honeybee_Honeybee component " + \
            "into canvas and try again."
            w = gh.GH_RuntimeMessageLevel.Warning
            ghenv.Component.AddRuntimeMessage(w, warning)
            return -1
            
        # don't customize this part
        hb_EPZone = sc.sticky["honeybee_EPZone"]
        hb_EPSrf = sc.sticky["honeybee_EPSurface"]
        hb_EPZoneSurface = sc.sticky["honeybee_EPZoneSurface"]
        hb_EPFenSurface = sc.sticky["honeybee_EPFenSurface"]
        hb_RADMaterialAUX = sc.sticky["honeybee_RADMaterialAUX"]
        hb_EPObjectsAux = sc.sticky["honeybee_EPObjectsAUX"]()
        
    else:
        print "You should first let Honeybee to fly..."
        w = gh.GH_RuntimeMessageLevel.Warning
        ghenv.Component.AddRuntimeMessage(w, "You should first let Honeybee to fly...")
        return
    
    # if the input is mesh, convert it to a surface
    try:
        # check if this is a mesh
        geometry.Faces[0].IsQuad
        # convert to brep
        geometry = rc.Geometry.Brep.CreateFromMesh(geometry, False)
    except:
        pass
    
    HBSurfaces = []
    originalSrfName = "person"
    for faceCount in range(person.Faces.Count):
    
    		# 0. check if user input a name for this surface
    		guid = str(uuid.uuid4())
    		number = guid.split("-")[-1]
    		
    		srfName = "".join(guid.split("-")[:-1])
    		
    		# 1. create initial surface
    		HBSurface = hb_EPZoneSurface(person.Faces[faceCount].DuplicateFace(False), number, srfName)
    		print(person.Faces[faceCount].NormalAt(0.5,0.5))
    		if person.Faces[faceCount].NormalAt(0.5,0.5) == rc.Geometry.Vector3d(0,0,-1):
    		    srfType = 2 #floor
    		elif person.Faces[faceCount].NormalAt(0.5,0.5) == rc.Geometry.Vector3d(0,0,1):
    		    srfType = 1 #roof
    		else:
    		    srfType = 0 #wall
  
    		# 1.1 check for surface type
    		if srfType!=None:
    			try:
    				# if user uses a number to input type
    				try: surfaceType = int(srfType)
    				except:
    					if float(srfType) == 0.5 or float(srfType) == 1.5 or float(srfType) == 2.25 or float(srfType) == 2.5 or float(srfType) == 2.75:
    						surfaceType = float(srfType)
    					else: pass
    				print "HBSurface Type has been set to " + HBSurface.srfType[float(srfType)]
    				
    				if surfaceType == 5.0:
    					warningMsg = "If you want to use this model for energy simulation, use addGlazing to add window to surfaces.\n" + \
    								 "It will be fine for Daylighting simulation though."
    					print warningMsg
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Remark, warningMsg)
    			except:
    				# user uses text as an input (e.g.: wall)
    				# convert it to a number if a valid input
    				surfaceType = srfType.ToUpper()
    				if surfaceType in HBSurface.srfType.keys():
    				   surfaceType = HBSurface.srfType[surfaceType.ToUpper()]
    				   print "HBSurface Type has been set to " + surfaceType.ToUpper()
    			
    			if surfaceType in HBSurface.srfType.keys():
    				acceptableCombination = [[1,3], [3,1], [0,5], [5,0], [1,5], [5,1], [4,0], [4,1], [4,2], [4,3], [0,4], [1,4], [2,4], [3,4]]
    				try:
    					if int(HBSurface.type) != surfaceType and [int(HBSurface.type), surfaceType] not in acceptableCombination:
    						warningMsg = "Normal direction of the surface is not expected for a " + HBSurface.srfType[surfaceType] + ". " + \
    									 "The surface is more likely a " + HBSurface.srfType[int(HBSurface.type)] + ".\n" + \
    									 "Honeybee won't overwrite the type so you may need to manually flip the surface."
    						print warningMsg
    					HBSurface.setType(surfaceType, isUserInput= True)
    					if surfaceType == 2.5 or surfaceType == 2.25:
    						HBSurface.setBC('ground', isUserInput= False)
    				except:
    					warningMsg = "You are using an old version of Honeybee_Honeybee! Update your files and try again."
    					print warningMsg
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    					return
    			
    		
    		# 1.2 assign boundary condition
    		EPBC = "Outdoors"
    		if EPBC!= None:
    			# only ground, adiabatic and outdoors is valid
    			validBC = ['ground', 'adiabatic', 'outdoors']
    			if EPBC.lower() in validBC:
    				try:
    					HBSurface.setBC(EPBC, isUserInput= True)
    					
    					if EPBC.lower()== "adiabatic":
    						if srfType == None and str(HBSurface.type).startswith('2'):
    							HBSurface.setType(2, False)
    						elif srfType == None and str(HBSurface.type).startswith('1'):
    							HBSurface.setType(3, False)
    						HBSurface.setEPConstruction(HBSurface.intCnstrSet[HBSurface.type])
    					
    					# change type of surface if BC is set to ground
    					if EPBC.lower()== "ground":
    						HBSurface.setType(int(HBSurface.type) + 0.5, isUserInput= True)
    					
    					if EPBC.lower()== "ground" or EPBC.lower()== "adiabatic":
    						HBSurface.setSunExposure('NoSun')
    						HBSurface.setWindExposure('NoWind')
    					
    					print "HBSurface boundary condition has been set to " + EPBC.upper()
    				except:
    					warningMsg = "You are using an old version of Honeybee_Honeybee! Update your files and try again."
    					print warningMsg
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    					return               
    			else:
    				print "HBSurface BOUNDARY CONDITION IS NOT VALID."
    		
    		# 1.3 assign construction for EnergyPlus
    		if EPConstruction!=None:
    			# if it is just the name of the material make sure it is already defined
    			if len(EPConstruction.split("\n")) == 1:
    				# if the material is not in the library add it to the library
    				if not hb_EPObjectsAux.isEPConstruction(EPConstruction):
    					warningMsg = "Can't find " + EPConstruction + " in EP Construction Library.\n" + \
    								"Add the construction to the library and try again."
    					print warningMsg
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    					return
    			else:
    				# it is a full string
    				if "CONSTRUCTION" in EPConstruction.upper():
    					added, EPConstruction = hb_EPObjectsAux.addEPObjectToLib(EPConstruction, overwrite = True)
    					
    					if not added:
    						msg = name + " is not added to the project library!"
    						ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, msg)
    						print msg
    						return
    				elif "MATERIAL" in EPConstruction.upper():
    					msg = "Your connected EPConstruction_ is just a material and not a full construction. \n You have to pass it through an 'EnergyPlus Construction' component before connecting it here."
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, msg)
    					print msg
    					return
    				else:
    					msg = "Your connected EPConstruction_ is not a valid construction."
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, msg)
    					print msg
    					return
    			
    			try:
    				HBSurface.setEPConstruction(EPConstruction)
    				print "HBSurface construction has been set to " + EPConstruction
    			except:
    				warningMsg = "You are using an old version of Honeybee_Honeybee! Update your files and try again."
    				print warningMsg
    				ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    				return                 
    			
    		# 1.4 assign RAD Material
    		RADMaterial = None
    		if RADMaterial!=None:
    			# if it is just the name of the material make sure it is already defined
    			if len(RADMaterial.split(" ")) == 1:
    				# if the material is not in the library add it to the library
    				if not hb_RADMaterialAUX.isMatrialExistInLibrary(RADMaterial):
    					warningMsg = "Can't find " + RADMaterial + " in RAD Material Library.\n" + \
    								"Add the material to the library and try again."
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    					return
    				
    				try:
    					HBSurface.setRADMaterial(RADMaterial)
    					print "HBSurface Radiance Material has been set to " + RADMaterial
    				except Exception, e:
    					print e
    					warningMsg = "You are using an old version of Honeybee_Honeybee! Update your files and try again."
    					print warningMsg
    					ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    					return
    				
    				addedToLib = True
    			else:
    				
    				# try to add the material to the library
    				addedToLib, HBSurface.RadMaterial = hb_RADMaterialAUX.analyseRadMaterials(RADMaterial, True)
    				
    			if addedToLib==False:
    				warningMsg = "Failed to add " + RADMaterial + " to the Library."
    				ghenv.Component.AddRuntimeMessage(gh.GH_RuntimeMessageLevel.Warning, warningMsg)
    				return
    		
    		HBSurfaces.append(HBSurface)
    		
    
    # add to the hive
    hb_hive = sc.sticky["honeybee_Hive"]()
    HBSurface  = hb_hive.addToHoneybeeHive(HBSurfaces, ghenv.Component)
    
    zoneID = str(uuid.uuid4())
    
    # default for isConditioned is True
    isConditioned=True
    zoneName = "person"
    bldgProgram = "person"
    zoneProgram = type
    
    
    HBZone = hb_EPZone(None, zoneID, zoneName.strip().replace(" ","_"), (bldgProgram, zoneProgram), isConditioned)
    
    for hbSrf in HBSurfaces:
        HBZone.addSrf(hbSrf)
        if hbSrf.shdCntrlZoneInstructs != []:
            HBZone.daylightCntrlFract = 1
            HBZone.illumSetPt = hbSrf.shdCntrlZoneInstructs[0]
            HBZone.GlareDiscomIndex = hbSrf.shdCntrlZoneInstructs[1]
            HBZone.glareView = hbSrf.shdCntrlZoneInstructs[2]
    
    # create the zone from the surfaces
    HBZone.createZoneFromSurfaces()
    
    if not HBZone.isClosed:
        message = "All of your HBSrfs must make a closed volume."
        print message
        w = gh.GH_RuntimeMessageLevel.Warning
        ghenv.Component.AddRuntimeMessage(w, message)
    
    HBZone  = hb_hive.addToHoneybeeHive([HBZone], ghenv.Component)
    
    return HBSurface, HBZone
    


if location != None:
    
    result= main(location, _EPConstruction)
    
    if result!=-1:
        HBSurface, HBZone = result