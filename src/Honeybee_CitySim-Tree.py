﻿# GH-CitySim: an interface to CitySim started by Giuseppe Peronato
#
# © All rights reserved. Ecole polytechnique fédérale de Lausanne (EPFL), Switzerland,
# Interdisciplinary Laboratory of Performance-Integrated Design (LIPID), 2016-2017
# Author: Giuseppe Peronato, <giuseppe.peronato@alumni.epfl.ch>
#
# This component is provided by Elioth (Egis Concept)
# Developer: Giuseppe Peronato <g.peronato@elioth.fr>
# 2019-2020

# CitySim is a software developed and distributed by the
# Laboratory of Solar Energy and Building Physics (LESO-PB)
# http://citysim.epfl.ch/

"""
This component creates a human thermal zone for outdoor confort modeling
based on the method developed by Dr. Silvia Coccolo 
http://dx.doi.org/10.5075/epfl-thesis-7756

@license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>

-

    
    Args:
        trunks: Datatree of trunk geometry
        leaves: DataTrees of horizontal surfaces (leaves)
        LAI: Leaf Area Index (sum of leave area surfaces divided by their projection to the ground)
        dir: directory of project
        name: title of project
        Write: Boolean to start
"""

ghenv.Component.Name = "Honeybee_CitySim-Tree"
ghenv.Component.NickName = 'CitySim-Tree'
ghenv.Component.Message = 'VER 0.0.2\nJUN_17_2020'
ghenv.Component.IconDisplayMode = ghenv.Component.IconDisplayMode.application
ghenv.Component.Category = "Honeybee"
ghenv.Component.SubCategory = "14 | CitySim"
ghenv.Component.AdditionalHelpFromDocStrings = "1"

import rhinoscriptsyntax as rs
import ghpythonlib.treehelpers as th
import Rhino as rc
import ghpythonlib.components as gc





leafstring = '<Leaf id="{}" ShortWaveReflectance="0.3" LongWaveEmissivity="0.95">\n'
vertexstring = '<V{} x="{}" y="{}" z="{}"/>\n'
trunkstring = '<Trunc id="{}" ShortWaveReflectance="0.3" LongWaveEmissivity="0.95">\n'
treestring = '<Tree id="{}" name="" key="TREE$TREE 10" leafAreaIndex="{}" leafWidth="0.1" leafDistance="1" deciduous="false" class="C3">\n'
xml = "<Trees>\n"


ReqInputs = True

if leaves == None:
    print "Add leaves"
    ReqInputs = False
    
if dir == None:
    print "Select a directory" #this is mandatory: no default
    ReqInputs = False
else:
    dir += "\\" #Add \ in case is missing
    
if name == None:
    name = "simulation" #default name
    



if ReqInputs:
    for t, trunk in enumerate(trunks):
        xml += treestring.format(t,LAI[t])
        leaf = leaves[t]
        xml+= leafstring.format(t*1000)
        for v, vertex in enumerate(leaf.Vertices):
                xml+= vertexstring.format(v,vertex.Location[0],vertex.Location[1],vertex.Location[2])
        xml += "</Leaf>"
        for te, trunkel in enumerate(gc.DeconstructBrep(trunk).faces):
            xml+= trunkstring.format(t*1000+100+te)
            for v, vertex in enumerate(trunkel.Vertices):
                    xml+= vertexstring.format(v,vertex.Location[0],vertex.Location[1],vertex.Location[2])
            xml += "</Trunc>"
        xml += "</Tree>"
    xml += "</Trees>"


def writeXML(xml, path, name):
    xmlpath = path+name+".trees"
    out_file = open(xmlpath,"w")
    out_file.write(xml)
    out_file.close()
    return xmlpath

if ReqInputs and Write:
    obj = writeXML(xml,dir,name)
    print "XML file created"
else:
    print "Set Write to true"

try:
    CSobj = obj
except:
    pass


