﻿# GH-CitySim: an interface to CitySim started by Giuseppe Peronato
#
# © All rights reserved. Ecole polytechnique fédérale de Lausanne (EPFL), Switzerland,
# Laboratory of Integrated Performance in Design (LIPID), 2016-2017
# Author: Giuseppe Peronato, <giuseppe.peronato@alumni.epfl.ch>
#
# This component is provided by Elioth (Egis Concept)
# Developer: Giuseppe Peronato <g.peronato@elioth.fr>
# 2019-2020

# CitySim is a software developed and distributed by the
# Laboratory of Solar Energy and Building Physics (LESO-PB)
# http://citysim.epfl.ch/

"""
This component reads the results from CitySim Comfort Modeling output

@license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>

-

    
    Args:
        _HBZones: The HoneyBee Zones of the project
        dir: directory of project
        name: title of project
        Run: Boolean to start
"""

ghenv.Component.Name = "Honeybee_CitySim-LoadComfort"
ghenv.Component.NickName = 'CitySim-LoadComfort'
ghenv.Component.Message = 'VER 0.0.1\nAVR_12_2020'
ghenv.Component.Category = "Honeybee"
ghenv.Component.SubCategory = "14 | CitySim"
try: ghenv.Component.AdditionalHelpFromDocStrings = "2"
except: pass

import rhinoscriptsyntax as rs
import Rhino as rc
import scriptcontext as sc
import uuid
import os
import ghpythonlib.treehelpers as th

import ghpythonlib as ghl


hb_hive = sc.sticky["honeybee_Hive"]()

if Run:
    geometry = []
    if dir != None:
        dir += "\\" #Add \ in case is missing
    
    HBO = [zone for zone in hb_hive.callFromHoneybeeHive(_HBZones) if zone.bldgProgram == "person"]
    file = os.path.join(dir,name+ "_CM.out")
    with open(file, "r") as f:
        lines = f.readlines()

    MRT = []
    COMFA = []
    ITS = []
    print(lines)
    for line in lines[1:]:
       chunks = line.split("\t")
       print(chunks)
       MRT.append([float(chunks[i]) for i in range(0,len(HBO)*3,3) if HBO[int(i/3)].zoneProgram == "MRT"])
       COMFA.append([float(chunks[i]) for i in range(1,len(HBO)*3,3) if HBO[int(i/3)].zoneProgram == "metrics"])
       ITS.append([float(chunks[i]) for i in range(2,len(HBO)*3,3) if HBO[int(i/3)].zoneProgram == "metrics"])

    if len(MRT) > 0 and len(MRT[0]) > 0:
        MRT = th.list_to_tree([list(x) for x in zip(*MRT)],source=[])
    else:
        MRT = "No humans of type 'MRT' in the _HBzones"
    if len(COMFA) > 0 and len(COMFA[0]) > 0:
        COMFA = th.list_to_tree([list(x) for x in zip(*COMFA)],source=[])
        ITS = th.list_to_tree([list(x) for x in zip(*ITS)],source=[])
    else:
        COMFA = "No person of type 'metrics' in the _HBzones"
        ITS = "No person of type 'metrics' in the _HBzones"
